/**
 * This file contains the set of the functions to redefine the tabulation
 */


var var_1, var_2, var_3;

function tabulationForFirstLine(event, leftArrow, rightArrow, tab) {

	assign(leftArrow, rightArrow, tab);

	tabulationPattern(event, getFocus, getFocus, getFocusIfDisabledThenPreventDefault);
};

function tabulationForSecondLine(event, leftArrow, rightArrow, tab) {

	assign(leftArrow, rightArrow, tab);

	tabulationPattern(event, getFocus, getFocus, getFocusThenPreventDefault);
};

function tabulationForComboBox(event, leftArrow, rightArrow, tab) {

	assign(leftArrow, rightArrow, tab);

	tabulationPattern(event, getFocusThenPreventDefault, getFocusThenPreventDefault, getFocusThenPreventDefault);
};

function tabulationPattern(event, function_1, function_2, function_3) {

	switch (event.keyCode) {
		case 38:
			function_1(var_1, event);
			break;
		case 40:
			function_2(var_2, event);
			break;
		case 9:
			function_3(var_3, event);
			break;
		default:
			// Do nothing
	};
};

function assign(leftArrow, rightArrow, tab) {

	var_1 = leftArrow;
	var_2 = rightArrow;
	var_3 = tab;
};

function getFocusIfDisabledThenPreventDefault(id, event) {

	if (!(document.getElementById(id).disabled)) {
		getFocus(id);
	};
	event.preventDefault();
};

function getFocusThenPreventDefault(id, event) {

	getFocus(id);
	event.preventDefault();
};

function getFocus(id, event) {

	document.getElementById(id).focus();
};
