var app = angular.module('myApp', []);

var sign = "";
var number = "";

app.controller('myCtrl', ['$scope', function($scope) {
	initialize();
	$scope.valueToInitiative = $scope.gamers[0].initiative;
	$scope.selectedGamerName = $scope.gamers[0].name;
	// To add a gamer
	$scope.addGamer = function() {
		if (($scope.gamerName != null) && ($scope.gamerName != "")) {
			if ($scope.gamerInitiative == null) {
				$scope.gamerInitiative = 0;
			}
			var suspension = "";
			if ($scope.gamerName.length > 8) {
				suspension = "…";
			}
			if ($scope.gamers[0].name == "Gamer") {
				$scope.gamers = [{"name":$scope.gamerName.substr(0, 8) + suspension, "title":$scope.gamerName, "valueTo":"", "initiative":Number($scope.gamerInitiative), "life":0, "check":false}];
				disable(false);
				$scope.valueToInitiative = "";
				$scope.selectedGamerName = $scope.gamerName.substr(0, 8) + suspension;
				$scope.gamerInitiative = null;
			} else {
				var isAbsent = true;
				for (var i = 0; i < $scope.gamers.length; i++) {
					if ($scope.gamerName == $scope.gamers[i].name) {
						isAbsent = false;
					};
				};
				if (isAbsent) {
					$scope.gamers.splice($scope.gamers.length, 0, {"name":$scope.gamerName.substr(0, 8) + suspension, "title":$scope.gamerName, "valueTo":"", "initiative":Number($scope.gamerInitiative), "life":0, "check":false});
					$scope.valueToInitiative = "";
					$scope.selectedGamerName = $scope.gamerName.substr(0, 8) + suspension;
					$scope.gamerInitiative = null;
				} else {
					alert('Le joueur nommé "' + $scope.gamerName + '" a déjà été créé.');
					if ($scope.gamerInitiative == "") {
						$scope.gamerInitiative = null;
					};
				};
			};
		};
		$scope.gamerName = null;
	};
	// To change the initiative (of a gamer)
	$scope.changeInitiative = function(event) {
		if ($scope.selectedGamerName != "Gamer") {
			for (var i = 0; i < $scope.gamers.length; i++) {
				if (($scope.selectedGamerName == $scope.gamers[i].name) && ("-" != $scope.valueToInitiative)) {
					$scope.gamers[i].initiative = Number($scope.gamers[i].initiative) + Number($scope.valueToInitiative);
					if ($scope.gamers[i].initiative < 0) {
						$scope.gamers[i].initiative = 0;
					};
					if ($scope.gamers[i].initiative > 99999999) {
						$scope.gamers[i].initiative = 99999999;
					};
					$scope.valueToInitiative = "";
				};
			};
		};
		event.preventDefault();
	};
	// To recycle gamer
	$scope.recycleBin = function() {
		for (var i = 0; i < $scope.gamers.length; i++) {
			if ($scope.gamers[i].check == true) {
				$scope.gamers.splice(i, 1);
				$scope.recycleBin();
			};
		};
		if ($scope.gamers.length > 0) {
			var minimum = $scope.gamers[0].index;
			for (var i = 0; i < $scope.gamers.length; i++) {
				if ($scope.gamers[i].index < minimum) {
					minimum = $scope.gamers[i].index;
				};
			};
			for (var i = 0; i < $scope.gamers.length; i++) {
				if (minimum == $scope.gamers[i].index) {
					$scope.valueToInitiative = $scope.gamers[i].valueTo;
					$scope.selectedGamerName = $scope.gamers[i].name;
				};
			};
		} else {
			initialize();
		};
		document.getElementById("gamerName").focus();
	};

	// To display in right format
	$scope.displayGamerInitiative = function(id, text) {
		$scope[id] = $scope.number(text);
	};
	$scope.displayValueToInitiative = function(id, text) {
		if (text[0] == "-") {
			sign = text[0];
		};
		$scope[id] = sign;
		number = $scope.number(text);
		if (number != null) {
			$scope[id] = sign + number;
		};
		sign = "";
	};
	$scope.displayLife = function(gamer) {
		for (var i = 0; i < $scope.gamers.length; i++) {
			if (gamer.name == $scope.gamers[i].name) {
				$scope.gamers[i].life = $scope.number($scope.gamers[i].life);
				if (null == $scope.gamers[i].life) {
					$scope.gamers[i].life = 0;
				};
			};
		};
	};
	// To validate a natural (integer) number
	$scope.number = function(text) {
		for (var i = 0; i < text.length; i++) {
			if (!(/\d/.test(text.charAt(i)))) {
				text = text.substr(0, i) + text.substr(i + 1, text.length - i + 1);
				i--;
			};
		};
		if (text.length == 0) {
			return null;
		} else {
			return Number(text);
		};
	};

	// To alternate the color of the lines of the gamers table
	/*
	$scope.getColor = function(gamer) {
		if (isMultipleOf(gamer.index, 2)) {
			return "#ccffcc";
		} else {
			return "";
		};
	};
	*/
	// To initialize the "view" table
	function initialize() {
		$scope.gamers = [{"name":"Gamer", "valueTo":"0", "initiative":0, "life":0, "check":false}];
		disable(true);
	};


	/**
	 * The tabulation for the third line
	 */
	// The column "life"::
	$scope.tabulationForLife = function(event, gamer) {

		if ((39 == event.keyCode) && (document.activeElement.value.length == document.activeElement.selectionStart)) {
			document.getElementsByClassName("check")[gamer.index - 1].focus();
		};
		$scope.tabulationCommon(event, gamer);
	};
	// The column "check"::
	$scope.tabulationForCheck = function(event, gamer) {

		if (37 == event.keyCode) {
			getSelection(gamer.index - 1, "life").focus();
			event.preventDefault();
		};
		$scope.tabulationCommon(event, gamer);
	};
	// The column "check"::
	$scope.tabulationCommon = function(event, gamer) {

		var keyCode = event.keyCode;
		switch (true) {
			case ((38 == keyCode) || (40 == keyCode)):
				getFocus(keyCode - 39, gamer, document.activeElement.classList[0]);
				event.preventDefault();
				break;
			case (9 == keyCode):
				document.getElementById("gamerName").focus();
				event.preventDefault();
				break;
			default:
				// Do nothing
		};
	};
	// To get the focus depending of the displacement:
	function getFocus(displacement, gamer, className) {

		for (var i = 0; i < $scope.gamers.length; i++) {
			if (gamer.index == $scope.gamers[i].index) {
				if (0 > displacement && 1 == gamer.index) {
					getSelection($scope.gamers.length - 1, className).focus();
				} else if (0 < displacement && $scope.gamers.length == gamer.index) {
					getSelection(0, className).focus();
				} else {
					getSelection((gamer.index - 1) + displacement, className).focus();
				};
				return;
			};
		};
	};
	// To get the selection
	function getSelection(y, x) {

		var point = document.getElementsByClassName(x)[y];

		if (/\d/.test(point.value)) {
			point.selectionStart = 0;
			point.selectionEnd = point.value.length;
		};

		return point;
	};


	// To see when the function "recycleBin" has focus 
	$scope.getBlues = function(index) {

		if ($scope.gamers.length == index--) {
			document.getElementsByClassName("check")[index - 1].focus();
		} else {
			document.getElementsByClassName("check")[index + 1].focus();
		};
		document.getElementsByClassName("check")[index].focus();
	};

}]);

// To test if a number is a multiple of another number
function isMultipleOf(dividend, divisor) {
	return ((dividend / divisor) - Math.trunc(dividend / divisor)) == 0;
};
// To disable the fields to change initiative
function disable(boolean) {
	document.getElementById("valueToInitiative").disabled = boolean;
	document.getElementById("changeInitiativeButton").disabled = boolean;
	document.getElementById("selectedGamerName").disabled = boolean;
	document.getElementById("gamerName").focus();
};
