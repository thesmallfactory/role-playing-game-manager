## "GESTIONNAIRE DES JEUX DE R�LE", version 1.0


###PR�SENTATION :

		Cette application a �t� con�ue pour permettre d'aider � la gestion des jeux de r�le.
		Elle peut pr�senter des lacunes ; toutefois, voici les actions qui (gr�ce � elle)
		ont �t� rendues automatiques :
			- l'ajout (ainsi que la suppression) d'un joueur,
			- le � calcul � de l'ordre de passage des joueurs,
			- l'ajout ou le retranchement de points d'� initiative �...

		Cette application, qui est une application Web, a �t� con�ue selon un motif d'archi-
		tecture MVC (Mod�le-Vue-Contr�leur) ; cependant, actuellement, elle ne comporte que
		la "vue" et le "contr�leur"...


###RECOMMANDATION :

		Pour un affichage optimal, utilisez Google Chrome (plut�t que Mozilla Firefox, par exemple).


� Cyril Marilier, Marseille, ao�t 2017.
